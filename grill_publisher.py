import json
import os

from mqtt_client import Client as MqttClient

MQTT_BROKER = os.environ["MQTT_BROKER"]
MQTT_USER = os.environ["MQTT_USER"]
MQTT_PASS = os.environ["MQTT_PASS"]


class GrillPublisher:
    status_topic = "grill_right/status"
    value_topic = "grill_right/channels"
    prev_ch1 = -99
    prev_ch2 = -99

    def __init__(self):
        self.client = MqttClient(MQTT_BROKER, MQTT_USER, MQTT_PASS)
        if not self.client.connect_mqtt(self.status_topic, "Stopped"):
            print("GrillPublisher: Cannot connect to MQTT")
            return

        print("GrillPublisher: Connected")
        self.client.publish(self.status_topic, "Started")

    def publish(self, ch1: float, ch2: float):
        pub_now = False
        if (self.prev_ch1 - 1) > ch1 or ch1 > (self.prev_ch1 + 1):
            pub_now = True
            self.prev_ch1 = ch1

        if (self.prev_ch2 - 1) > ch2 or ch2 > (self.prev_ch2 + 1):
            pub_now = True
            self.prev_ch2 = ch2

        if pub_now:
            temp_obj = {
                'ch1': round(ch1, 1),
                'ch2': round(ch2, 1)
            }
            self.client.publish(self.value_topic, json.dumps(temp_obj))

    def disconnect(self):
        self.client.publish(self.status_topic, "Stopped")

