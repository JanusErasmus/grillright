import time
import socket
from paho.mqtt import client as mqtt_client

KEEP_ALIVE_TIMEOUT = 600


class Client:
    def __init__(self, broker: str, username: str, password: str,
                 keep_alive_topic: str = None, keep_alive_message: str = None):
        self.client = mqtt_client.Client()
        self.broker = broker
        self.port = 1883
        self.username = username
        self.password = password
        self.keep_alive_topic = keep_alive_topic
        self.keep_alive_message = keep_alive_message
        self.keep_alive_tick = time.time()
        self.connected = False
        print(f"Keep alive: {self.keep_alive_tick}")

    def __del__(self):
        print("Disconnect")
        self.client.disconnect()

    def connect_mqtt(self,
                     will_topic: str = None,
                     will_message: str = None) -> bool:

        def on_connect(client, userdata, flags, rc):
            if rc == 0:
                print("Connected to MQTT Broker!")
                self.connected = True
            else:
                print("Failed to connect, return code %d\n", rc)

        def on_disconnect(client, userdata, rc):
            print("Disconnected from MQTT Broker!")
            self.connected = False

        if will_topic and will_message:
            print(f"Will set: {will_topic} - {will_message}")
            self.client.will_set(will_topic, will_message, 0, False)

        self.client.username_pw_set(self.username, self.password)
        self.client.on_connect = on_connect
        self.client.on_disconnect = on_disconnect
        try:
            err = self.client.connect(self.broker, self.port, keepalive=KEEP_ALIVE_TIMEOUT)
        except ConnectionRefusedError:
            print("Server not available")
            return False
        except socket.gaierror as e:
            print(f"Cannot connect: {e}")
            return False

        if err == 0:
            self.connected = True
            return True
        else:
            print(f"Cannot connect to {self.broker}: {err}")

        return False

    def keep_alive(self):
        if self.keep_alive_topic and self.keep_alive_message:
            self.keep_alive_tick = time.time()
            self.client.publish(self.keep_alive_topic, self.keep_alive_message)

    def publish(self, topic: str, msg: str) -> bool:
        result = self.client.publish(topic, msg)

        status = result[0]
        if status == 0:
            self.keep_alive_tick = time.time()
            # print(f"Send `{msg}` to topic `{topic}`")
            return True
        else:
            print(f"Failed to send message to topic {topic}")

        return False

    def subscribe(self, topic, call_back):
        print(f"Subscribed {topic}")
        self.client.subscribe(topic)
        self.client.on_message = call_back

    def run(self):
        if not self.connected:
            return False

        if self.keep_alive_tick + (KEEP_ALIVE_TIMEOUT - 30) < time.time():
            self.keep_alive()
        self.client.loop()
        return True
