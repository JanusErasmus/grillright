# Grill-Right Monitor

[Oregon Scientific AW133 Grill-Right Bluetooth BBQ Thermometer](https://www.oregonscientificstore.com/p-6-oregon-scientific-aw133-grill-right-bluetooth-bbq-thermometer.aspx)

Python script to monitor the Dual Thermometer

## First time in Windows

Connect once with Bluetooth LE explorer, installed from the Microsoft store

## Running

 - Create a virtual environment
 - Install packages in requirements.txt
 - Run and see temperatures in terminal

### A javascript example

[BBQ Thermometer](https://gist.github.com/mattdsteele/0fa9cafc4a95738181137547eae21fc8?permalink_comment_id=3274030)