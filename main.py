import asyncio
from bleak import BleakScanner, BleakClient
import struct
import datetime

from grill_publisher import GrillPublisher

MAC = "C1:0C:64:38:11:C6"
TEMPERATURE_SERVICE = "2899fe00-c277-48a8-91cb-b29ab0f01ac4"
TEMPERATURE_REQ = "28998e03-c277-48a8-91cb-b29ab0f01ac4"
TEMPERATURE_CH1 = "28998e10-c277-48a8-91cb-b29ab0f01ac4"
TEMPERATURE_CH2 = "28998e11-c277-48a8-91cb-b29ab0f01ac4"

up_time = datetime.datetime.now()

grill_publisher = GrillPublisher()


def parse_temperature(data: bytearray):
    temp_raw = data[12:14]
    temperature_f = struct.unpack("<H", temp_raw)[0] / 10
    return ((temperature_f - 32) * 5) / 9


def show_temp(channel: str, data: bytearray):
    temperature = parse_temperature(data)
    time_now = datetime.datetime.now().strftime("%H:%M:%S")
    print(f"{time_now} {channel}: {temperature:0.1f}")


ch1_updated = False
ch2_updated = False
ch1 = 0
ch2 = 0


def handle_notify_data(sender, data):
    global ch1_updated, ch1, ch2_updated, ch2
    if sender.uuid == TEMPERATURE_REQ:
        print(f"REQ: {data}")
    if sender.uuid == TEMPERATURE_CH1:
        # show_temp("CH1", data)
        ch1 = parse_temperature(data)
        ch1_updated = True
    if sender.uuid == TEMPERATURE_CH2:
        # show_temp("CH2", data)
        ch2 = parse_temperature(data)
        ch2_updated = True

    if ch1_updated and ch2_updated:
        ch1_updated = False
        ch2_updated = False
        time_now = datetime.datetime.now().strftime("%H:%M:%S")
        time_up = (datetime.datetime.now() - up_time).seconds
        print(f"{time_now} {time_up: 4} {ch1:03.1f}℃ {ch2:0.1f}℃")
        grill_publisher.publish(ch1, ch2)


running = True


def client_disconnected(client):
    global running, grill_publisher
    print(f"Disconnected: {client}")
    running = False  # exit(1)
    grill_publisher.disconnect()


async def monitor_temperature():
    global running, grill_publisher
    # List all devices
    # devices = await BleakScanner.discover()
    # for d in devices:
    #     print(d)

    device = await BleakScanner.find_device_by_address(
        MAC)
    if device is None:
        print("could not find device with address '%s'", MAC)
        grill_publisher.disconnect()
        return
    print(f"Connecting: {device}")
    async with BleakClient(device, disconnected_callback=client_disconnected, timeout=30) as client:
        print(f"Connected {device}")
        # List all services
        # services = await client.get_services()
        # for service in services:
        #     print(service)

        # temperature_service = client.services[TEMPERATURE_SERVICE]
        # List all characteristics
        # for char in temperature_service.characteristics:
        #     print(char)

        await client.start_notify(TEMPERATURE_REQ, handle_notify_data)
        await asyncio.sleep(0.5)
        await client.start_notify(TEMPERATURE_CH1, handle_notify_data)
        await asyncio.sleep(0.5)
        await client.start_notify(TEMPERATURE_CH2, handle_notify_data)
        # temp_request = client.services.get_characteristic(TEMPERATURE_REQ)
        # print(temp_request)

        while running:
            await asyncio.sleep(1)


def main():
    print("Grill-Right monitor")
    asyncio.run(monitor_temperature())


if __name__ == "__main__":
    main()
